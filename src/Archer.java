public class Archer extends Character{

    /**
     * @deprecated
     * @param target the charcter to attack
     */
    public void attack(Character target) {
        if(stamina >= 5){
            int damage = 15 + stamina / 2;
            target.takeDamage(damage);
            stamina-=5;
        }
    }

    public Archer(String name, Race race) {
        super(40, 10, 20, name, race);
    }
}
